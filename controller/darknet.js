const { spawn } = require('child_process');

module.exports = function darknetProcessing(
	commandLocation,
	commandDetails,
	destination,
	responseResults,
	res,
	extraArgs = [],
	cwd
) {
	const darknet = spawn(commandLocation, [ ...commandDetails.commandArgs, `${destination}`, ...extraArgs ], { cwd });
	console.log(commandLocation);
	let output = '';
	let stdOut = '';

	darknet.stdout.on('data', (data) => {
		console.log(`stdout: ${data}`);
		output += data + '\n';
		stdOut += data + '\n';
	});

	darknet.stderr.on('data', (data) => {
		console.log(`stderr: ${data}`);
		output += data + '\n';
	});

	darknet.on('close', (code) => {
		if (commandDetails.nnType == 'nightmare') {
			const outputLines = output.split(/\n+/);
			console.log('outputLines: ', outputLines);
			const lastLine = outputLines[outputLines.length - 2];
			console.log('lastLine: ', lastLine);
			const lastLineSplit = lastLine.split(/\s+/);
			console.log('lastLineSplit: ', lastLineSplit);
			const lastChunk = lastLineSplit[lastLineSplit.length - 1];
			console.log('lastChunk: ', lastChunk);
			responseResults.nightmareLocation = lastChunk.substr('nightmare/'.length);
		}
		console.log(`child process exited with code ${code}`);
		output += `Exit code ${code}`;

		responseResults.data = stdOut || output;

		res.send(responseResults);
	});
};
