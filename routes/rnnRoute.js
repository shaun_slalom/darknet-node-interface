const path = require('path');
const darknetProcessing = require('../controller/darknet');
const darknetFolder = require('../config').darknetFolder;

const rnnRoute = (app) => {
  app.post('/rnn', function(req, res) {
    //if (!req.files)
    //    return res.status(400).send('No files were uploaded.');
    const extraArgs = [];

    const commandDetails = JSON.parse(req.body.commandDetails);
    console.log(commandDetails);

    const commandLocation = path.resolve(darknetFolder, commandDetails.command);
    console.log('Command Location', commandLocation);

    const responseResults = {
      data: '',
      nnType: commandDetails.nnType
    };

    console.log('rnnSeed is:', req.body.rnnSeed);
    if (req.body.rnnSeed) {
      extraArgs.push('-seed');
      extraArgs.push(req.body.rnnSeed);
    }
    darknetProcessing(commandLocation, commandDetails, null, responseResults, res, extraArgs, darknetFolder);
  });
};

module.exports = rnnRoute;
