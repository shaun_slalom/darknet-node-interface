const imageRoute = require('./imageRoute');
const uploadRoute = require('./uploadRoute');
const rnnRoute = require('./rnnRoute');

const routeHandler = (app) => {
  imageRoute(app);
  uploadRoute(app);
  rnnRoute(app);
};

module.exports = routeHandler;
