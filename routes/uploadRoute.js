const path = require('path');
const darknetProcessing = require('../controller/darknet');
const darknetFolder = require('../config').darknetFolder;

const uploadRoute = (app) => {
  app.post('/upload', function(req, res) {
    //if (!req.files)
    //    return res.status(400).send('No files were uploaded.');
    const extraArgs = [];

    const commandDetails = JSON.parse(req.body.commandDetails);
    console.log(commandDetails);

    const commandLocation = path.resolve(darknetFolder, commandDetails.command);
    console.log('Command Location', commandLocation);

    const responseResults = {
      data: '',
      nnType: commandDetails.nnType
    };

    // The name of the input field (i.e. "uploadedFile") is used to retrieve the uploaded file
    let uploadedFile = req.files.uploadedFile;
    const extension = uploadedFile.name.substr(uploadedFile.name.lastIndexOf('.'));

    responseResults.extension = extension;

    //Picking out a location to put the uploaded file
    let originalDestination = `${__dirname}/${uploadedFile.name}`;

    // Keep track of destination file might need to change it if we are using
    // certain commands (nightmare outputs a formatted file, detector outputs another)
    let destination = originalDestination;

    //If we are not doing a prediction just save the original file as the prediciton output for display
    if (!commandDetails.generatesPrediction) {
      destination = `${darknetFolder}/original${extension}`;
    }

    // Use the mv() method to place the file somewhere on your server
    uploadedFile.mv(destination, function(err) {
      if (err) return res.status(500).send(err);

      if (commandDetails.nnType == 'nightmare') {
        extraArgs.push('10');
        extraArgs.push('-prefix');
        extraArgs.push('nightmare');
      }
      if (commandDetails.nnType == 'detector' && commandDetails.confidenceThreshold) {
        extraArgs.push('-thresh');
        extraArgs.push(commandDetails.confidenceThreshold / 100);
      }
      darknetProcessing(commandLocation, commandDetails, destination, responseResults, res, extraArgs, darknetFolder);

      //res.sendFile()
    });
  });
};

module.exports = uploadRoute;
