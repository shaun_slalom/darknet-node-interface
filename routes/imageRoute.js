const appConfig = require('../config');

const { darknetFolder } = appConfig;

const imageRoute = (app) => {
	app.get('/images/:nnType/:filename/:extension/', (req, res) => {
		console.log(req.params);
		const { extension, filename, nnType } = req.params;
		if (req.params.nnType == 'detector') {
			res.sendFile(`${darknetFolder}/predictions.png`);
		}
		if (req.params.nnType == 'classifier') {
			res.sendFile(`${darknetFolder}/original${extension}`);
		}
		if (req.params.nnType == 'nightmare') {
			res.sendFile(`${darknetFolder}/nightmare/${filename}.png`);
		}
	});
};

module.exports = imageRoute;
