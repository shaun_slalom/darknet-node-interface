const path = require('path');
// Useful for going into sibling directory for running darknet commands
// could have used 'path' but simple enough
const darknetFolder = path.resolve(__dirname, '/Users/shaun.husain/Development/darknet/');
const port = 3001;

module.exports = {
	darknetFolder,
	port
};
