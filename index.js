const express = require('express');
const cors = require('cors');
const fileUpload = require('express-fileupload');

const app = express();

const routeDefiner = require('./routes');
const appConfig = require('./config');

app.use(cors());
app.use(fileUpload());

routeDefiner(app);

const { darknetFolder, port } = appConfig;

app.listen(port, '0.0.0.0', () => console.log(`Listening on port ${port}`));
